package com.company.email.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDate;

@Table(name = "EMAIL_NEW_ITEM")
@Entity(name = "email_NewItem")
@NamePattern("%s|caption")
public class NewItem extends StandardEntity {
    private static final long serialVersionUID = -4306627244675154428L;

    @Column(name = "DATE_")
    private LocalDate date;

    @Column(name = "CONTENT")
    private String content;

    @Column(name = "CAPTION")
    private String caption;

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}