-- begin EMAIL_NEW_ITEM
create table EMAIL_NEW_ITEM (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    DATE_ date,
    CONTENT varchar(255),
    CAPTION varchar(255),
    --
    primary key (ID)
)^
-- end EMAIL_NEW_ITEM
