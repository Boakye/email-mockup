package com.company.email.web.screens.newitem;

import com.haulmont.cuba.gui.screen.*;
import com.company.email.entity.NewItem;

@UiController("email_NewItem.edit2")
@UiDescriptor("new-item-edit2.xml")
@EditedEntityContainer("newItemDc")
@LoadDataBeforeShow
public class NewItemEdit2 extends StandardEditor<NewItem> {
}