package com.company.email.web.screens.newitem;

import com.haulmont.cuba.core.app.EmailService;
import com.haulmont.cuba.core.global.EmailInfo;
import com.haulmont.cuba.core.global.EmailInfoBuilder;
import com.haulmont.cuba.gui.Dialogs;
import com.haulmont.cuba.gui.components.Component;
import com.haulmont.cuba.gui.components.DialogAction;
import com.haulmont.cuba.gui.model.DataContext;
import com.haulmont.cuba.gui.screen.*;
import com.company.email.entity.NewItem;

import javax.inject.Inject;
import java.util.Collections;

@UiController("email_NewItem.edit")
@UiDescriptor("new-item-edit.xml")
@EditedEntityContainer("newItemDc")
@LoadDataBeforeShow
public class NewItemEdit extends StandardEditor<NewItem> {

    private boolean justCreated;

    @Inject
    protected EmailService emailService;

    @Inject
    protected Dialogs dialogs;

    @Subscribe
    public void onInitEntity(InitEntityEvent<NewItem> event) {
        justCreated = true;
    }

    @Subscribe(target = Target.DATA_CONTEXT)
    public void onPostCommit(DataContext.PostCommitEvent event) {
        if (justCreated) {
            dialogs.createOptionDialog()
                    .withCaption("Email")
                    .withMessage("Send the news item by email?")
                    .withType(Dialogs.MessageType.CONFIRMATION)
                    .withActions(
                            new DialogAction(DialogAction.Type.YES) {
                                @Override
                                public void actionPerform(Component component) {
                                    sendByEmail();
                                }
                            },
                            new DialogAction(DialogAction.Type.NO)
                    )
                    .show();
        }
    }

    private void sendByEmail() {
        NewItem newsItem = getEditedEntity();
        EmailInfo emailInfo = EmailInfoBuilder.create()
                .setAddresses("kampaabeng@excenit.com,mboakye@excneit.com")
                .setCaption(newsItem.getCaption())
                .setFrom(null)
                .setTemplatePath("com/company/email/core/template/new_item.txt")
                .setTemplateParameters(Collections.singletonMap("newItem", newsItem))
                .build();
        emailService.sendEmailAsync(emailInfo);
    }
}

